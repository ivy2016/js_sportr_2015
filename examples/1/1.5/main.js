function run() {

    // Type definition
    function Building(name) {
        this.name = name;
    }
    var building = new Building('Pirsenteret');
    // building --> Building.prototype --> Object.prototype --> null


    // todo: add prototype property getName
    // todo log different properties
    //return;


    // Extending Type
    function Farm(name, location) {
        Building.call(this, name);
        this.location = location;
    }

    Farm.prototype = Object.create(Building.prototype);
    var kviset = new Farm('Kviset', 'Trondheim');
    // kviset --> Farm.prototype --> Building.prototype --> Object.prototype --> null


    // todo add prototype property getLocation
    // todo log different properties
    // todo explain instanceof
    return;


    // todo: building prototype - add getRandomNumber()





}