function run() {

    // traditional function + execution
    var msg = 'red';
    var myFnc = function() {

        console.log(msg);

    };
    myFnc();
    return;

    // self-executing anonymous function
    (function() {

        var msg = 'blue';
        console.log(msg);

    })();


    return;
    var someVar = 'Welcome!';
    setTimeout(function() {
        console.log(someVar);
    }, 1000);
    someVar = 'Goodbye.';
}