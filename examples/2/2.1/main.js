function run() {

    var templates = {
        row: '<div><strong>{{id}}</strong>: {{name}}</div>'
    };

    var countries = [
        { id: 1, name: 'Norway' },
        { id: 2, name: 'Sweden' },
        { id: 3, name: 'Netherlands' },
        { id: 4, name: 'Belgium' },
        { id: 5, name: 'Germany' }
    ];

    var html = [], parsed;
    countries.forEach(function(data) {

        parsed = kite(templates.row, data);
        html.push(parsed);
    });

    $('body').append( html.join('') );
}