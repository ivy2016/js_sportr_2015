function run() {

    // todo recursive template parsing, including backend example
    var templates = {
        table:      '<table>{{children}}</table>',
        row:        '<tr><td>{{children}}{{content}}</td></tr>',
        message:    '<div class="message">{{children}}{{content}}</div>',
        icon:       '<img class="icon" src="tux.png" />',
        button:     '<button type="button">{{children}}{{content}}</button>',
        label:     '<span class="label">{{children}}{{content}}</span>'
    };

    var parseTemplates = function(collection) {

        var html = [];

        collection.forEach(function(t) {

            if(t.data.children && t.data.children instanceof Array) {
                t.data.children = parseTemplates(t.data.children);
            }

            html.push( kite(templates[t.name], t.data) );
        });
        return html.join('');
    }

    $.ajax({
        url: 'json.xhr.php',
        success: function(response) {
            var json = JSON.parse(response);
            $('#content').append( parseTemplates(json) );
        }
    });
}