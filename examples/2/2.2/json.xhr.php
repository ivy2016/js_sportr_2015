<?php

$collection = [];

// Create elements
$icon = new Template('icon');

$label = (new Template('label'))
    ->setProperty('content', 'fancy label');

$button = (new Template('button'))
    ->setProperty('content', 'click me')
    ->addChild($icon);

// Message #1
array_push($collection, (new Template('message'))
    ->setProperty('content', '51% of people think stormy weather affects cloud computing.')
    ->addChild($icon)
    ->addChild($label)
);

// Message #2
array_push($collection, (new Template('message'))
    ->setProperty('content', 'You can change your language on Facebook to "Pirate".')
    ->addChild($button)
);

// Table
$iconLabel = clone $label;
$iconLabel->addChild($icon);
$max = 10;
$table = new Template('table');
for($i = 1; $i <= $max; $i++) {
    $table->addChild((new Template('row'))
        ->addChild($button)
        ->addChild($i == $max ? $iconLabel : $label)
        ->setProperty('content', 'table row')
    );
}
$collection[] = $table;

// output json
echo json_encode($collection);









class Template {

    public $name;
    public $data = [];

    public function __construct($name) {
        $this->name = $name;
        return $this;
    }

    public function setProperty($key, $value) {
        $this->data[$key] = $value;
        return $this;
    }

    public function addChild(Template $child) {
        if(!array_key_exists('children', $this->data)) {
            $this->data['children'] = [];
        }
        array_push($this->data['children'], $child);
        return $this;
    }
}