(function($) {

    var Tools = {};

    Tools.log = function(output) {
        console.log.apply(console, arguments);
    };

    Tools.assertEqual = function(desc, valueOne, valueTwo) {
        this.log(desc, valueOne === valueTwo);
    }

    Tools.assertTrue = function(desc, expression) {
        this.log(desc, expression === true);
    }

    Tools.showIndex = function(nrActive) {

        $(function() {

            var templates = {
                list: '<ul class="contents">{{#items}}<li data-href="{{href}}" class="{{? isHeader}} header{{/?}}{{? active}} active{{/?}}">{{nr}}. {{content}}</li>{{/items}}</ul>'
            }

            var createUri = function(nr) {
                if(nr % 1 == 0) return '';
                var h = location.href,
                    dir = 'examples';
                 return [h.substring(0, h.indexOf(dir) + dir.length), Math.floor(nr), nr].join('/');
            }

            var items = [
                { nr: 1, 'content': 'A good look at JavaScript' },
                { nr: 1.05, content: 'Scopes' },
                { nr: 1.1, content: 'Closures' },
                { nr: 1.2, content: 'Global namespace pollution' },
                { nr: 1.3, content: 'Invocation methods' },
                { nr: 1.4, content: 'Self-executing anonymous functions' },
                { nr: 1.5, content: 'Prototyping' },
                { nr: 1.6, content: 'Organizing lots of code' },
                { nr: 1.7, content: 'ECMAScript 6 ("ES.next")' },
                { nr: 2, 'content': 'Client-side templating' },
                { nr: 2.1, content: 'Introduction' },
                { nr: 2.2, content: 'Example' },
                { nr: 3, content: 'Node.js/Socket.IO' },
                { nr: 3.1, content: 'Introduction' },
                { nr: 3.2, content: 'Example' }
            ];
            var active;
            for(var k in items) {
                active = items[k].nr == nrActive;
                items[k].active = active;
                if(active) document.title = items[k].content;
                items[k].isHeader = items[k].nr % 1 == 0;
                items[k].href = createUri(items[k].nr);
            }

            $('body').append( kite(templates.list, { items: items }) );

            $('li[data-href]').bind('click', function() {
                var h = $(this).data('href');
                if(!h) return;
                window.location.href = h;
            });

        });
    }

    window.Tools = Tools;

})(jQuery, undefined);