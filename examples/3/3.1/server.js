var io = require('socket.io').listen(3737);
console.log('Up and running.');

// new connections
io.on('connection', function(socket) {

    // emit to sender:              socket.emit
    // emit to everyone:            io.emit
    // emit to all, except origin:  socket.broadcast.emit

    socket.on('ping', function(data) {

        console.log('RECEIVED ping from ' + data.city);

        var msg = (new Date).getTime() + ' - Hello, ' + data.city + '!';
        io.emit('pong', { message: msg });
    });
});