function run() {

    var templates = { block: '<div class="block" id="{{id}}" style="background-color:{{color}};">{{id}}</div>' },
        client = io.connect('http://localhost:3838'),
        colors = [
            '#588C7E', '#F2E394', '#F2AE72', '#D96459', '#8C4646',
            '#2A7F76', '#BDE2DE', '#6AA9A3', '#0A6057', '#00302B'
        ],
        color = colors[(Math.floor(Math.random()*(colors.length-1)))];

    // append "my block" to body
    $('body').append(kite(templates.block, { id: 'myBlock', color: color }));

    // notify server that we exist ( + send our color)
    client.emit('presence', { color: color });

    // listener: when server tells us there is a new client, add that client as a block to the DOM
    client.on('newClient', function(data) {
        $('body').append(kite(templates.block, { id: data.id, color: data.color }));
    });

    // listener: when server tells us client has a new position, update the DOM
    client.on('newClientPosition', function(data) {
        $('#' + data.id).css({
            left: data.position.left,
            top: data.position.top
        });
    });

    client.on('someoneLeftTheBuilding', function(data) {
        $('#' + data.id).remove();
    });

    // make "my block" draggable
    $('#myBlock').draggable({
        drag: function(event, ui) {
            // notify server that position of "my block" has changed
            client.emit('newBlockPosition', { position: ui.position });
        }
    });
}