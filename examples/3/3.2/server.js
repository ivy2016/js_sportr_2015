var io = require('socket.io').listen(3838);
console.log('Up and running.');

var uid = 100;
var blocks = {};
var Block = function(id, color) {
    this.id = id;
    this.left = 0;
    this.top = 0;
    this.color = color;
}

// new connections
io.on('connection', function(socket) {

    // emit to sender:              socket.emit
    // emit to everyone:            io.emit
    // emit to all, except origin:  socket.broadcast.emit

    var uniqueId = ['block', ++uid].join('_');

    socket.on('presence', function(data) {

        var block;
        for(var _uid in blocks) {
            block = blocks[_uid];
            socket.emit('newClient', { id: _uid, color: block.color });
            socket.emit('newClientPosition', { id: _uid, position: {
                left: block.left,
                top: block.top
            }});
        }

        blocks[uniqueId] = new Block(uniqueId, data.color);
        socket.broadcast.emit('newClient', { id: uniqueId, color: data.color });
    });

    socket.on('disconnect', function() {

        delete blocks[uniqueId];
        socket.broadcast.emit('someoneLeftTheBuilding', { id: uniqueId });

    });

    //client.emit('newBlockPosition', { position: ui.position });
    socket.on('newBlockPosition', function(data) {
        blocks[uniqueId].left = data.position.left;
        blocks[uniqueId].top = data.position.top;
        socket.broadcast.emit('newClientPosition', { position: data.position, id: uniqueId });
    });

});